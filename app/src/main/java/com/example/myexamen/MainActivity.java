package com.example.myexamen;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Room;

import com.example.myexamen.Appi.ApiService;
import com.example.myexamen.Appi.Converter;
import com.example.myexamen.Appi.RequestModel;
import com.example.myexamen.database.MDatabase;
import com.example.myexamen.database.MyDataBaseDao;
import com.example.myexamen.Appi.WeathModel;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity {

    MDatabase myDataBaseDao;
    List<WeathModel> weathModelList;
    MyDataBaseDao dao;

    Disposable subscribe;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        myDataBaseDao = Room
                .databaseBuilder(this, MDatabase.class, "database")
                .allowMainThreadQueries()
                .build();

        dao = myDataBaseDao.getDatabaseDao();

        if (dao.countAll() > 0) {
            subscribe = dao.selectAll()
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(new Consumer<List<WeathModel>>() {

                        @Override
                        public void accept(List<WeathModel> weathModels) throws Exception {
                            weathModelList = weathModels;

                        }
                    });
        } else {
            ApiService.getFactsByWeather("Kharkiv", "89dea4795f4998f2e6a06040f1ecc578")
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(new Consumer<RequestModel>() {
                        @Override
                        public void accept(RequestModel requestModel) throws Exception {
                            weathModelList = Converter.convertRequest(requestModel);
                        }

                    });
        }

    }

    @Override
    protected void onDestroy() {
        subscribe.dispose();
        super.onDestroy();
    }
}
