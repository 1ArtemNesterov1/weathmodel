package com.example.myexamen.Appi;

import io.reactivex.Observable;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Query;

//https://api.openweathermap.org/data/2.5/weather?q=Kharkiv&appid=89dea4795f4998f2e6a06040f1ecc578
public class ApiService {

    private static final String API = "https://openweathermap.org/";
    private static PrivateApi privateApi;

    static {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(API)
                .client(client)
                .build();

        privateApi = retrofit.create(PrivateApi.class);

    }


    public static Observable<RequestModel> getFactsByWeather(String city, String id) {
        return privateApi.getFactsByWeather(city, id);
    }


    public interface PrivateApi {
        @GET("data/2.5/weather")
        Observable<RequestModel> getFactsByWeather(@Query("q") String city, @Query("appid") String id);
    }


}
