package com.example.myexamen.Appi;

import androidx.room.Entity;

@Entity
public class WeathModel{
    String weather;
    String description;
    String timeDawn;
    String timeSunset;
    String chanceRain;
    String speedWind;
    String temperature;

    String minTemperature;
    String maxTemperature;

    public WeathModel(String weather, String description, String timeDawn, String timeSunset, String chanceRain, String speedWind, String temperature, String minTemperature, String maxTemperature) {
        this.weather = weather;
        this.description = description;
        this.timeDawn = timeDawn;
        this.timeSunset = timeSunset;
        this.chanceRain = chanceRain;
        this.speedWind = speedWind;
        this.temperature = temperature;
        this.minTemperature = minTemperature;
        this.maxTemperature = maxTemperature;
    }

    public String getMinTemperature() {
        return minTemperature;
    }

    public void setMinTemperature(String minTemperature) {
        this.minTemperature = minTemperature;
    }

    public String getMaxTemperature() {
        return maxTemperature;
    }

    public void setMaxTemperature(String maxTemperature) {
        this.maxTemperature = maxTemperature;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getWeather() {
        return weather;
    }

    public String getTemperature() {
        return temperature;
    }

    public void setTemperature(String temperature) {
        this.temperature = temperature;
    }

    public void setWeather(String weather) {
        this.weather = weather;
    }

    public String getTimeDawn() {
        return timeDawn;
    }

    public void setTimeDawn(String timeDawn) {
        this.timeDawn = timeDawn;
    }

    public String getTimeSunset() {
        return timeSunset;
    }

    public void setTimeSunset(String timeSunset) {
        this.timeSunset = timeSunset;
    }

    public String getChanceRain() {
        return chanceRain;
    }

    public void setChanceRain(String chanceRain) {
        this.chanceRain = chanceRain;
    }

    public String getSpeedWind() {
        return speedWind;
    }

    public void setSpeedWind(String speedWind) {
        this.speedWind = speedWind;
    }
}
