package com.example.myexamen.adapter;


import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.myexamen.Appi.WeathModel;
import com.example.myexamen.R;


import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MyRecyclerAdapter extends RecyclerView.Adapter<MyRecyclerAdapter.ViewHolder> {

    List<WeathModel> weathModels;
    Context context;
    OnDrinkClickListener listener;

    public MyRecyclerAdapter(List<WeathModel> weathModels, Context context, OnDrinkClickListener listener) {
        this.weathModels = weathModels;
        this.context = context;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.recycler_view_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(position);
    }

    @Override
    public int getItemCount() {
        return weathModels.size();
    }

    public interface OnDrinkClickListener {
        void onItemClick(WeathModel weathModel);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.temperature)
        TextView temperature;
        @BindView(R.id.minTemp)
        TextView minTemp;
        @BindView(R.id.maxTemp)
        TextView maxTemp;
        @BindView(R.id.iv_imageOnDay)
        ImageView imageView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(final int position) {
            temperature.setText(weathModels.get(position).getTemperature());
            minTemp.setText(weathModels.get(position).getMinTemperature());
            maxTemp.setText(weathModels.get(position).getMaxTemperature());

          //  Glide.with(context)
            //        .load(weathModels.get(position).())
              //      .error(new ColorDrawable(Color.RED))
                //    .placeholder(R.drawable.gradient)
                  //  .into(imageView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(weathModels.get(position));
                }
            });
        }
    }
}
